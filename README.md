# README #

Angular 4 Service for determining mobile device type.
* Written in TypeScript

### How do I get set up? ###

- Import the module:
	- `import { DeviceModule } from 'ng-device-service';`
- Add it as an import:
	- `imports: [ DeviceModule ]`
- Inject it in the constructor of your component:
	- `constructor(private deviceService: DeviceService) {}`
- Call the methods:
	- `this.deviceService.isiIOS();`

### Methods ###
* isiOS() -
* isAndroid() -
* isBlackberry() -
* isWindowsPhone() -
* isMobile() - 


### Contribution guidelines ###

* Code
* Write tests
* Pull Request

### Improvement ideas, general non-sense... ###

* Brant Wellons <Brant@BrantWellons.com>