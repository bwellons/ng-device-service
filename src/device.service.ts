import 'reflect-metadata';
import { Injectable } from '@angular/core';

@Injectable()
export class DeviceService {
	constructor() {}

	isiOS() {
		return /iPod|iPad|iPhone/i.test(navigator.userAgent);
	}

	isAndroid() {
		return /android/i.test(navigator.userAgent);
	}

	isBlackberry() {
		return /blackberry/i.test(navigator.userAgent);
	}

	isWindowsPhone() {
		return /windows phone/i.test(navigator.userAgent)
	}

	isMobile() {
		return this.isiOS() 
			|| this.isAndroid()
			|| this.isBlackberry()
			|| this.isWindowsPhone();
	}

}